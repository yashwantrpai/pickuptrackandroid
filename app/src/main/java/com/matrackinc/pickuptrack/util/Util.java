package com.matrackinc.pickuptrack.util;

import com.matrackinc.pickuptrack.R;

public class Util {

    public static boolean isValidUrl(String url, String[] urls){
        for(int it = 0; it < urls.length; it++){
            if(urls[it].equals(url)){
                return true;
            }
        }
        return false;
    }
}
