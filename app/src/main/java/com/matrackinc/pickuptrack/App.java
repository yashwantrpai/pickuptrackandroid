package com.matrackinc.pickuptrack;

import android.content.Context;
import android.content.SharedPreferences;

public class App extends android.support.multidex.MultiDexApplication {
    public static final String PREFS_NAME = "logPrefFile";
    public static Context appContext;

    @Override
    public void onCreate() {

        super.onCreate();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("logEnabled", false);
        editor.commit();
        appContext = this;
    }

    public static Context getContext(){
        return appContext;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}