package com.matrackinc.pickuptrack.util;

import android.content.SharedPreferences;

import com.matrackinc.pickuptrack.activity.SplashScreen;
import com.matrackinc.pickuptrack.pojo.PreferencePojo;

public class PreferenceUtil {

    public static void setPreference(PreferencePojo preferencePojo){
        SharedPreferences.Editor editor = SplashScreen.getEditor();

        editor.putString("username", preferencePojo.getUsername());
        editor.putString("password", preferencePojo.getPassword());
        editor.putString("flag", preferencePojo.getFlag());
        editor.putString("authcode", preferencePojo.getAuthcode());
        editor.commit();
    }

    public static PreferencePojo getPreference(){
        SharedPreferences pref = SplashScreen.getPref();
        PreferencePojo preferencePojo = new PreferencePojo();

        preferencePojo.setUsername(pref.getString("username", ""));
        preferencePojo.setPassword(pref.getString("password", ""));
        preferencePojo.setFlag(pref.getString("flag", "0"));
        preferencePojo.setAuthcode(pref.getString("authcode", ""));

        return preferencePojo;
    }
}
