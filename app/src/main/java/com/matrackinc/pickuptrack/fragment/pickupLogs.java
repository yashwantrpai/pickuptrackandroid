package com.matrackinc.pickuptrack.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matrackinc.pickuptrack.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class pickupLogs extends Fragment {

    View rootView;
    public pickupLogs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.pickup_logs, container, false);

        // Call pickup logs api
        // List down using CardView the response, similar to Uber's past trips

        return rootView;
    }
}
