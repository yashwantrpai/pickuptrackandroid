package com.matrackinc.pickuptrack.util;

public class ConvertUtil {    //

    public static Double getDouble(String str, String defaultValue) {
        double value = 0.0;
        try{
            if (str != null) {
                value = Double.parseDouble(str);
            } else {
                value = Double.parseDouble(defaultValue);
            }
        } catch(Exception ex) {
            ex.printStackTrace();

        }
        return value;
    }
}
