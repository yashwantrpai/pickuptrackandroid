package com.matrackinc.pickuptrack.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matrackinc.pickuptrack.CreatePostString;
import com.matrackinc.pickuptrack.CustomHttpClient;
import com.matrackinc.pickuptrack.R;
import com.matrackinc.pickuptrack.activity.HomeActivity;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class changePassword extends Fragment{

    public static String old_password_str, new_password_str, confirm_new_password_str;
    public static ConnectivityManager connectivityManager;
    public static Context context, activity_context;
    public static String response = "";
    public static SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public LinearLayout indicator_layout;
    public ImageView indicator_img;
    private static final String TAG = "ChangePassword";
    public static TextView changePassword_title;

    public changePassword() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.change_password, container, false);

        context = this.getActivity().getApplicationContext();
        activity_context = this.getActivity();

        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        changePassword_title = (TextView) rootView.findViewById(R.id.change_password_title);
        changePassword_title.setText(R.string.change_password);
        final EditText old_password_txt = (EditText) rootView.findViewById(R.id.old_password);
        old_password_txt.setHint(R.string.old_password);
        final EditText new_password_txt = (EditText) rootView.findViewById(R.id.new_password);
        new_password_txt.setHint(R.string.new_password);
        final EditText confirm_new_password_txt = (EditText) rootView.findViewById(R.id.re_enter_new_password);
        confirm_new_password_txt.setHint(R.string.re_enter_password);

        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // retain this fragment
        setRetainInstance(true);

        Button submit = (Button) rootView.findViewById(R.id.submit);
        submit.setText(R.string.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!old_password_txt.getText().toString().equals("") && !new_password_txt.getText().toString().equals("") && !confirm_new_password_txt.getText().toString().equals("")) {
                    old_password_str = old_password_txt.getText().toString().trim();
                    new_password_str = new_password_txt.getText().toString().trim();
                    confirm_new_password_str = confirm_new_password_txt.getText().toString().trim();

                    if (new_password_str.equals(confirm_new_password_str)) {
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                            resetPassword();
                        } else {
                            Toast.makeText(context, R.string.internet_connectivity_check, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(R.string.confirm_password_correctly)
                                .setCancelable(false)
                                .setNeutralButton(getActivity().getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                } else {
                    Toast.makeText(activity_context, R.string.enter_valid_data_for_fields, Toast.LENGTH_SHORT).show();
                }
                // } else {
                //  Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.enter_valid_data_for_fields:R.string.enter_valid_data_for_fields_esp, Toast.LENGTH_LONG).show();
                // }
            }
        });

        ImageView back = (ImageView) rootView.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!getActivity().isFinishing()) {
                    String CURRENT_TAG = HomeActivity.TAG_HELP;
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                    transaction.replace(R.id.frame, new help(), CURRENT_TAG);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void resetPassword(){
        final String changePassword = context.getString(R.string.change_password_url);
        String resetPasswordUrl = null;
        try {
            String currentServer = pref.getString("currentServer", "");
            resetPasswordUrl = currentServer + changePassword;
        } catch (Exception e){
            e.printStackTrace();
        }

        ArrayList params = new ArrayList();
        Pair pair = new Pair("operation", "changePassword");
        params.add(pair);
        pair = new Pair("authcode", pref.getString("authcode", ""));
        params.add(pair);
        pair = new Pair("oldpassword", old_password_str);
        params.add(pair);
        pair = new Pair("newpassword", new_password_str);
        params.add(pair);

        String postString = CreatePostString.createPostString(params);
        resetPasswordTask resetPasswordTask = new resetPasswordTask();
        resetPasswordTask.execute(resetPasswordUrl, postString);
    }

    public static class resetPasswordTask extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Boolean responsefailed = false;
            if (result != null && result != "" && !response.isEmpty()) {

                if(response.contains("updated New password")){
                    Log.d("changePassword", "Password changed successfully");
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity_context);
                    builder.setMessage(R.string.password_changed_successfully)
                            .setCancelable(false)
                            .setNeutralButton(activity_context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    responsefailed = true;
                }
            } else {
                responsefailed = true;
            }

            if (responsefailed) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity_context);
                builder.setMessage(R.string.reset_password_failed)
                        .setCancelable(false)
                        .setNeutralButton(activity_context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }
}