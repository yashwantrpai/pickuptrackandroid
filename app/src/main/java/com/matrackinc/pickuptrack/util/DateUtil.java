package com.matrackinc.pickuptrack.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//This is test commit
public class DateUtil {
    public static String covertToCivilianTime(String time) {
        Date dateFormat = null;
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        try {
            dateFormat = format.parse(time);
            return new SimpleDateFormat("KK:mm a").format(dateFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }
}
