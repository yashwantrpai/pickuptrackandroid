package com.matrackinc.pickuptrack.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.matrackinc.pickuptrack.CustomHttpClient;
import com.matrackinc.pickuptrack.R;
import com.matrackinc.pickuptrack.WebkitCookieManagerProxy;
import com.squareup.okhttp.OkHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;

public class SplashScreen extends AppCompatActivity {

    public static OkHttpClient OkHTTPClient;
    public static WebkitCookieManagerProxy coreCookieManager;
    public static String response;
    public static String currentServer="";
    public static Context context;
    public static Activity activity;
    public static String TAG = "SplashScreen";
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;

    public static String getCurrentServer() {
        return currentServer;
    }

    public boolean checkServer(String url) {
        Boolean exists = false;
        InetAddress address;
        try {
            address = InetAddress.getByName(new URL(url).getHost());
            SocketAddress sockaddr = new InetSocketAddress(address, 80);
            // Create an unbound socket
            Socket sock = new Socket();

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            int timeoutMs = 5000;   // 5 seconds
            sock.connect(sockaddr, timeoutMs);
            exists = true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            Context context = SplashScreen.this.getApplicationContext();
            Toast.makeText(context, "Please check your internet connectivity!", Toast.LENGTH_SHORT).show();
        }
        return exists;
    }
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        activity = this;

        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorNotification));
        }
        // This method will be executed once the timer is over
        // Start your app main activity
        new checkServerTask().execute();
        // close this activity
    }

    public static SharedPreferences getPref() {
        return pref;
    }

    public static SharedPreferences.Editor getEditor() {
        return editor;
    }

    public static OkHttpClient getClient() {
        if(OkHTTPClient == null) {
            OkHTTPClient = new OkHttpClient();
            // initialize WebkitCookieManagerProxy, set Cookie Policy, create android.webkit.webkitCookieManager
            coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(coreCookieManager);
            OkHTTPClient.setCookieHandler(coreCookieManager);
        }
        return OkHTTPClient;
    }

    public void getSessionCookie(Context activity_context){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        String url = pref.getString("currentServer", "") +"/gpstracking/matrackApp/EnableLogin_app2.php";
        JSONObject params = new JSONObject();
        try {
            params.put("username", pref.getString("username", ""));
            params.put("password", pref.getString("password", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if(activity_context == SplashScreen.this){
                SplashScreen.getSessionLoadMaps t1 = new SplashScreen.getSessionLoadMaps();
                t1.execute(url, params.toString());
            }else { SplashScreen.validateUserTask t2 = new SplashScreen.validateUserTask();
                t2.execute(url, params.toString());
            }
        }else{
            Context context = activity_context.getApplicationContext();
            Toast.makeText(context, "Please check your internet connectivity!", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(activity_context, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    public class validateUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Log.d(TAG, "Enable_login response: "+response);
        }
    }

    public class checkServerTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute(){
            //bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                String urls[] = getResources().getStringArray(R.array.url);
                while(true){
                    if(checkServer(urls[0])){
                        currentServer=urls[0];
                        editor.putString("currentServer", urls[0]);
                        editor.commit();
                        break;
                    }
                    else if(checkServer(urls[1])){
                        currentServer=urls[1];
                        editor.putString("currentServer", urls[1]);
                        editor.commit();
                        break;
                    }
                }
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            //return res;
            return "";
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            //bar.setVisibility(View.GONE);

            Log.d(TAG, "CurrentServer "+currentServer);
            if (pref.getString("flag", "0").equals("1") && (pref.getString("keepSession", "1").equalsIgnoreCase("1"))) {
                getSessionCookie(SplashScreen.this);
            }
            else {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        }
    }

    public class getSessionLoadMaps extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Intent i = new Intent(SplashScreen.this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }
}
