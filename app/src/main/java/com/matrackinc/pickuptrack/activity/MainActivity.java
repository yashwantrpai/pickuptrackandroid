package com.matrackinc.pickuptrack.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.os.AsyncTask;
import java.lang.String;
import java.util.ArrayList;

import android.content.Intent;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.matrackinc.pickuptrack.CreatePostString;
import com.matrackinc.pickuptrack.CustomHttpClient;
import com.matrackinc.pickuptrack.R;
import com.matrackinc.pickuptrack.pojo.PreferencePojo;
import com.matrackinc.pickuptrack.util.PreferenceUtil;
import com.matrackinc.pickuptrack.util.Util;
import com.suke.widget.SwitchButton;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    Button mButton;
    EditText email, passcode;
    String response = "true";
    private static final String TAG = "MyActivity";
    Context context;
    private ProgressDialog pdia;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create new file with new values

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        activity = this;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorNotification));
        }

        email = (EditText) findViewById(R.id.email_address);
        passcode = (EditText) findViewById(R.id.passcode);

        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!email.getText().toString().equals("") && !passcode.getText().toString().equals("")) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        login();
                    } else {
                        Toast.makeText(context, "Please check your internet connectivity..", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AlertDialog WrongPasswordAlert = new AlertDialog.Builder(MainActivity.this).create();
                    WrongPasswordAlert.setTitle("Empty credentials");
                    WrongPasswordAlert.setMessage("Please enter credentials to login");
                    WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    WrongPasswordAlert.show();
                }
            }
        });
    }

    public void login() {
        String urls[] = getResources().getStringArray(R.array.url);
        String currentServer = pref.getString("currentServer", "");
        if(Util.isValidUrl(currentServer, urls)) {
            String loginUrl = currentServer + getString(R.string.login_url);
            Log.d("login url",loginUrl);
            ArrayList params = new ArrayList();
            Pair pair = new Pair("operation", "login");
            params.add(pair);
            pair = new Pair("username", email.getText().toString());
            params.add(pair);
            pair = new Pair("password", passcode.getText().toString()); // android.telephony.TelephonyManager.getDeviceId()
            params.add(pair);

            String postString = CreatePostString.createPostString(params);
            Log.i("param", postString);
            loginTask loginTask = new loginTask();
            loginTask.execute(loginUrl, postString);
        } else {
            Toast.makeText(context, "Server connection issue..", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(context, SplashScreen.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( pdia!=null && pdia.isShowing() ){
           pdia.cancel();
        }
    }

    private class loginTask extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!isFinishing() && android.os.Build.VERSION.SDK_INT > 19) {
                pdia = new ProgressDialog(context);
                pdia.setMessage("Validating credentials..");
                pdia.setCancelable(false);
                pdia.show();
            }
        }

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                Log.d("Response",res);
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(!isFinishing()) {
                if (pdia != null && pdia.isShowing()) {
                    pdia.dismiss();
                }
            }
            response = result;

            if (response != null && !response.equals("")) {
                Log.d(TAG, "Enable_login response: "+response);
                JSONObject respjson = new JSONObject();
                    try {
                        respjson = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        //Log.d(TAG, response);
                        if (response != null && response.contains("true")) {
                            JSONObject data = new JSONObject();
                            try {
                                data = respjson.getJSONObject("data");
                                String authcode = data.getString("authcode");
                                String trackpermission=data.getString("trackpermission");
                                String reportpermission=data.getString("reportpermission");
                                if(pref.getString("keepSession", "1").equals("1")){
                                    editor.putString("savedUsername", email.getText().toString());
                                    editor.putString("savedPassword", passcode.getText().toString());
                                    editor.commit();
                                }

                                PreferencePojo preferencePojo = new PreferencePojo(email.getText().toString(),passcode.getText().toString(),"1",authcode);
                                PreferenceUtil.setPreference(preferencePojo);


                                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            AlertDialog WrongPasswordAlert = new AlertDialog.Builder(MainActivity.this).create();
                            WrongPasswordAlert.setTitle("Login Failed");
                            WrongPasswordAlert.setMessage("Please type correct credentials to login");
                            WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            WrongPasswordAlert.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }//close onPostExecute
        }// close loginTask

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }
}
