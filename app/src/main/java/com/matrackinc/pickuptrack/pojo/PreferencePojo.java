package com.matrackinc.pickuptrack.pojo;

public class PreferencePojo {

    String username;
    String password;
    String flag;
    String authcode;

    public PreferencePojo() {
        this.username = "";
        this.password = "";
        this.flag = "0";
        this.authcode = "";
    }

    public PreferencePojo(String username, String password, String flag, String authcode) {
        this.username = username;
        this.password = password;
        this.flag = flag;
        this.authcode = authcode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }
}
