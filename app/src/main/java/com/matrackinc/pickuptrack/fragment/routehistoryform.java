package com.matrackinc.pickuptrack.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.matrackinc.pickuptrack.activity.HomeActivity;
import com.matrackinc.pickuptrack.util.MyItem;
import com.matrackinc.pickuptrack.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class routehistoryform extends Fragment {

    public routehistoryform() {
        // Required empty public constructor
    }

    public static Context context;
    // Track History Date-range activity
    private static final String TAG = "Trackhistory1-dateRange";
    // public static ArrayList values = new ArrayList();
    DatePickerDialog datePickerDialog;
    String vehicle_str;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Button submit;

    public static MyItem getClickedMarker() {
        return getClickedMarker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.routehistory1, container, false);
        context = this.getContext();

        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        submit = (Button) rootView.findViewById(R.id.submit);

        if(pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "routehistoryform")) {
            editor.putString("date", "");
            editor.putString("vehicle", "");
            editor.putString("curfile", "routehistoryform");
        }
        editor.commit();

        // Setting default values
        // Prefilling start date and end date

        final TextView date_text = (TextView) rootView.findViewById(R.id.date_text);
        final Spinner Vehicle = (Spinner) rootView.findViewById(R.id.vehicle_text);
        Vehicle.setSelection(0);

        date_text.setOnClickListener(new View.OnClickListener() {
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH), month = c.get(Calendar.MONTH), year = c.get(Calendar.YEAR);
            String dt;
            @Override
            public void onClick(View view) {
                if(pref.getString("startdate", "") != null && pref.getString("startdate", "") != ""){
                    String[] date_split = pref.getString("startdate", "").split("/");
                    day = Integer.parseInt(date_split[1]);
                    month = Integer.parseInt(date_split[0]);
                    year = Integer.parseInt(date_split[2]);
                }

                // Click action
                datePickerDialog = new DatePickerDialog(context, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                        Log.d("Selected Date", selectedDay+" "+selectedMonth+" "+selectedYear);

                        Date date = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.set(selectedYear,selectedMonth,selectedDay);
                        date.setTime(cal.getTime().getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.date_format));
                        dt = dateFormat.format(date);
                        date_text.setText(dt);
                        // values.set(0,date_text.getText());
                        editor.putString("startdate", date_text.getText().toString());
                        editor.commit();
                        Log.d("Selected Date", dt);
                    }
                }, year, month - 1, day);
                // Set Max date and Min date for add date
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                // datePickerDialog.setTitle("Pick start date");
                datePickerDialog.show();
            }
        });

        // End Date
        Date date = new Date();
        SimpleDateFormat curFormater = new SimpleDateFormat(getString(R.string.date_format));
        String Date = curFormater.format(date);
        Log.d(TAG, Date);
        date_text.setText(Date);

        String drivers_str = pref.getString("drivers", "");
        String drivers[] = null;
        try {
            if(drivers_str != null && drivers_str!= ""){
                JSONArray drivers_arr = new JSONArray(drivers_str);
                if(drivers_arr != null){
                    drivers = new String[drivers_arr.length()];
                    for(int i = 0; i < drivers_arr.length(); i++){
                        drivers[i] = drivers_arr.get(i).toString();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(drivers != null && drivers.length > 0) {
            ArrayAdapter<CharSequence> driver_adapter = new ArrayAdapter<CharSequence>(context, R.layout.spinner_item, drivers);
            driver_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
            Vehicle.setAdapter(driver_adapter);
            Vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    vehicle_str = adapterView.getItemAtPosition(i).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Log.w(TAG, "locationTrack.Name is null");
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialogBuilder.setTitle(R.string.data_sync_issue);
            alertDialogBuilder.setMessage(R.string.re_login_and_try_again);
            alertDialogBuilder.setNeutralButton(getActivity().getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialogBuilder.show();
        }

        editor.putString("date", Date);
        editor.putString("vehicle", vehicle_str);
        editor.commit();

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if((date_text.getText().toString().matches(""))){
                    ArrayList no_values = new ArrayList();
                    no_values.add("");
                    no_values.add("");
                    if(date_text.getText().toString().matches("")){
                        no_values.set(0,"Date");
                    }
                    AlertDialog Alert = new AlertDialog.Builder(context).create();
                    Alert.setMessage(Html.fromHtml(getContext().getResources().getString(R.string.please_fill_values)+": <br>"+no_values.get(0).toString()+"<br>"+no_values.get(1).toString()));
                    Alert.setButton(AlertDialog.BUTTON_NEUTRAL, getActivity().getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    Alert.show();
                }
            }
        });

        return rootView;
    }
}
