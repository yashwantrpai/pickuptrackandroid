package com.matrackinc.pickuptrack.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.AlertDialog;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;

import com.matrackinc.pickuptrack.CreatePostString;
import com.matrackinc.pickuptrack.CustomHttpClient;
import com.matrackinc.pickuptrack.R;
import com.matrackinc.pickuptrack.activity.HomeActivity;

import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class help extends Fragment {
    Switch loggingSwitch;
    public static final String PREFS_NAME = "logPrefFile";
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Activity activity;
    ArrayList<Integer> Image_icon = new ArrayList<>();
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> Image = new ArrayList<>();
    RecyclerView Alert_recycler_View;
    SettingsAdapter Adapter;
    public AlertDialog.Builder ratingAlert, selectLanguageAlert;
    public AlertDialog ratingDialog, languageSelectDialog;
    public View ratingView, languageSelectView;
    public LinearLayout ratingLayout;
    public RadioGroup radioGroup;
    public String response;
    public boolean isLanguageModified = false;

    public help() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        activity = getActivity();
        pref = activity.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        Image_icon.clear();
        text.clear();
        Image.clear();

        Alert_recycler_View = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        Adapter = new SettingsAdapter(Image_icon, text, Image);
        Alert_recycler_View.setAdapter(Adapter);
        Alert_recycler_View.setHasFixedSize(true);
        Alert_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));

        setNewLayout(R.drawable.changepassword, getString(R.string.change_password), R.drawable.next);
        setNewLayout(R.drawable.mailus, getString(R.string.contact_us), R.drawable.next);
        setNewLayout(R.drawable.rateus, getString(R.string.rate_us), R.drawable.next);

        return rootView;
    }

    public void sendMail(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","support@tracking-gpsandtrack.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Pickup Track Support mail");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_mail)));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void inflateRatingLayout(){
        LayoutInflater inflater = getLayoutInflater();
        ratingView = inflater.inflate(R.layout.ratinglayout, null);
        ratingLayout = (LinearLayout) ratingView.findViewById(R.id.rating_layout);
        ratingLayout.setVisibility(View.VISIBLE);
        final TextView rating_layout_title = (TextView) ratingView.findViewById(R.id.rating_layout_title);
        rating_layout_title.setText(R.string.rate_us);
        final TextView rate_me = (TextView) ratingView.findViewById(R.id.rate_me);
        rate_me.setText(R.string.rating_text);
        final ImageView close = (ImageView) ratingView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingDialog.dismiss();
            }
        });
        final RatingBar ratingBar = (RatingBar) ratingView.findViewById(R.id.ratingBar);
        final EditText ratingText = (EditText) ratingView.findViewById(R.id.ratingText);
        ratingText.setHint(R.string.review_hint);
        final Button submitRating = (Button) ratingView.findViewById(R.id.submit);
        submitRating.setText(R.string.submit);

        ratingAlert = new AlertDialog.Builder(activity);
        // this is set the view from XML inside AlertDialog
        ratingAlert.setView(ratingView);
        // disallow cancel of AlertDialog on click of back button and outside touch
        ratingAlert.setCancelable(false);
        ratingDialog = ratingAlert.create();

        submitRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rating = ratingBar.getRating();
                String reviewText = ratingText.getText().toString();
                submitReview(rating, reviewText);
            }
        });

        ratingDialog.show();
    }

    public void submitReview(float rating, String review){
        SharedPreferences pref;
        pref = activity.getSharedPreferences("DataStore", Context.MODE_PRIVATE);
        String currentServer = pref.getString("currentServer", "");
        String sendUserFeedbackUrl = currentServer + R.string.star_rating_url;

        ArrayList params = new ArrayList();
        Pair pair = new Pair("operation", "SubmitRatings");
        params.add(pair);
        pair = new Pair("authcode", pref.getString("authcode", ""));
        params.add(pair);
        pair = new Pair("ratings", rating);
        params.add(pair);
        pair = new Pair("comments", review);
        params.add(pair);

        String postString = CreatePostString.createPostString(params);
        sendUserFeedbackTask sendUserFeedbackTask = new sendUserFeedbackTask();
        sendUserFeedbackTask.execute(sendUserFeedbackUrl, postString);
    }

    public class sendUserFeedbackTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
            } catch (Exception e) {

            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Boolean responsefailed = false;

            if (result != null && result != "" && !result.isEmpty()) {
                response = result;
                Log.d("sendFeedback", response);

                if(response.contains("success")){
                    Log.d("sendFeedback", "Feedback sent successfully");
                    if(ratingDialog.isShowing()){
                        ratingLayout.setVisibility(View.GONE);
                        LinearLayout thanksLayout = (LinearLayout) ratingView.findViewById(R.id.thanks_layout);
                        thanksLayout.setVisibility(View.VISIBLE);
                        TextView thank_you_text = (TextView) ratingView.findViewById(R.id.thanksText);
                        thank_you_text.setText(R.string.thank_you_text);
                        Button backBtn = ratingView.findViewById(R.id.backBtn);
                        backBtn.setText(R.string.back);
                        backBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ratingDialog.dismiss();
                            }
                        });
                    }
                } else {
                    responsefailed = true;
                }
            } else {
                responsefailed = true;
            }

            if(responsefailed){
                if(ratingDialog.isShowing()){
                    ratingDialog.dismiss();
                }
                Toast.makeText(activity, R.string.send_feedback, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setNewLayout(int image_icon, String txt, int image){
        Image_icon.add(image_icon);
        text.add(txt);
        Image.add(image);

        Adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.MyViewHolder> {

        ArrayList<Integer> Image_icon = new ArrayList<>();
        ArrayList<String> Text = new ArrayList<>();
        ArrayList<Integer> Image = new ArrayList<>();

        public SettingsAdapter(ArrayList<Integer> Image_icon, ArrayList<String> text, ArrayList<Integer> Image)
        {
            this.Image_icon = Image_icon;
            this.Text = text;
            this.Image = Image;
        }

        @Override
        public SettingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View card_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new SettingsAdapter.MyViewHolder(card_view);
        }

        @Override
        public void onBindViewHolder(SettingsAdapter.MyViewHolder holder, final int position)
        {
            try {
                holder.text.setText(Text.get(position).toString());
                holder.image_icon.setImageResource(Image_icon.get(position));
                holder.image.setImageResource(Image.get(position));

                holder.image.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View view) {
                        action(position);
                    }
                });
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return Image_icon.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;
            ImageView image_icon, image;

            public MyViewHolder(View view) {
                super(view);
                image_icon = (ImageView) view.findViewById(R.id.image_icon);
                text = (TextView) view.findViewById(R.id.text);
                image = (ImageView) view.findViewById(R.id.next);
            }
        }
    }

    public void loadChangePasswordFragment(){
        Fragment targetFragment = new help();
        String CURRENT_TAG = HomeActivity.TAG_CHANGE_PASSWORD;
        targetFragment = new changePassword();
        editor.putString("CURRENT_TAG", HomeActivity.TAG_CHANGE_PASSWORD);
        editor.commit();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.replace(R.id.frame, targetFragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void action(int position){
        switch (position){
            case 0:
                loadChangePasswordFragment();
                break;

            case 1:
                sendMail();
                break;

            case 2:
                inflateRatingLayout();
                break;

            default:
                break;
        }
    }
}