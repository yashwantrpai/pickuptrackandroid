package com.matrackinc.pickuptrack.util;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
    private final LatLng mPosition;
    private int mIndex;
    private double mLatitude;
    private double mLongitude;
    private double mDirection;
    private String mDate;
    private String mTime;
    private String mSpeed;
    private String mVoltage;
    private String mEvent;
    private String mMileage;
    private String mFuel;

    public MyItem(LatLng mPosition, int mIndex, double mLatitude, double mLongitude, String mDate, String mTime, String mSpeed, String mVoltage, String mEvent, double mDirection, String mFuel, String mMileage) {
        this.mPosition = mPosition;
        this.mIndex = mIndex;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mDate = mDate;
        this.mTime = mTime;
        this.mSpeed = mSpeed;
        this.mVoltage = mVoltage;
        this.mEvent = mEvent;
        this.mDirection = mDirection;
        this.mMileage = mMileage;
        this.mFuel = mFuel;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int mIndex) {
        this.mIndex = mIndex;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getSpeed() {
        return mSpeed;
    }

    public void setSpeed(String mSpeed) {
        this.mSpeed = mSpeed;
    }

    public String getVoltage() {
        return mVoltage;
    }

    public void setVoltage(String mVoltage) {
        this.mVoltage = mVoltage;
    }

    public String getEvent() {
        return mEvent;
    }

    public void setEvent(String mEvent) {
        this.mEvent = mEvent;
    }

    public double getDirection() {
        return mDirection;
    }

    public void setDirection(double mDirection) {
        this.mDirection = mDirection;
    }

    public String getMileage() {
        return mMileage;
    }

    public void setMileage(String mMileage) {
        this.mMileage = mMileage;
    }

    public String getFuel() {
        return mFuel;
    }

    public void setFuel(String mFuel) {
        this.mFuel = mFuel;
    }
}
