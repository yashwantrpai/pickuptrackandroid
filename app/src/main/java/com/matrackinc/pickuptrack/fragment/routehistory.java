package com.matrackinc.pickuptrack.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.matrackinc.pickuptrack.CreatePostString;
import com.matrackinc.pickuptrack.CustomHttpClient;
import com.matrackinc.pickuptrack.activity.HomeActivity;
import com.matrackinc.pickuptrack.activity.SplashScreen;
import com.matrackinc.pickuptrack.util.ConvertUtil;
import com.matrackinc.pickuptrack.util.DateUtil;
import com.matrackinc.pickuptrack.util.MyItem;
import com.matrackinc.pickuptrack.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import com.matrackinc.pickuptrack.util.Util;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class routehistory extends Fragment implements OnMapReadyCallback {

    public routehistory() {
        // Required empty public constructor
    }

    String response = "";
    JSONArray result_json = new JSONArray();
    public String  authcode = "";
    public String Address;
    public double lat[], lng[], direction[];
    public String date[], time[], speed[], voltage[], event[], fuel[], mileage[], address[];
    public static View rootView;
    MapView mapView;
    private GoogleMap googleMap = null;
    public static ImageView zoomplus, zoomminus;
    public static Button rb_normal, rb_satellite, rb_hybrid;
    public static String TAG = "Route History";
    private LatLngBounds.Builder builder;
    private LatLngBounds bounds;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public static Boolean isDestroyed = false;
    static TextView p_bar;
    private ClusterManager<MyItem> mClusterManager;
    public static MyItem clickedMarker;
    public static DefaultClusterRenderer<MyItem> mRenderer;
    public static String start_date = "", end_date = "", data_point = "", driver = "", client = "";
    LatLngBounds initialbounds = null;
    public Context context;
    public static String google_maps_api;
    public static LinearLayout progressLayout;
    public static ProgressBar progressBar;
    public static TextView progressText;

    public static TextView trackhistory_title;
    public static ImageView back_btn;

    public static MyItem getClickedMarker() {
        return clickedMarker;
    }

    public void plotPoints(String output){
        response = output;
        p_bar.setVisibility(View.VISIBLE);
        Boolean no_data = false, data_issue = false;
        if(response != null && !response.equals("")) {
            try {
                JSONArray resp_json = new JSONArray(response);
                result_json = resp_json;
                //resp_json.getString(1).replace('/',' ');
                // Log.d(TAG, String.valueOf(resp_json));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String line = "";
            if (result_json != null) {
                if (result_json.length() > 1) {
                    lat = new double[result_json.length() - 1];
                    lng = new double[result_json.length() - 1];
                    date = new String[result_json.length() - 1];
                    time = new String[result_json.length() - 1];
                    speed = new String[result_json.length() - 1];
                    direction = new double[result_json.length() - 1];
                    voltage = new String[result_json.length() - 1];
                    event = new String[result_json.length() - 1];
                    fuel = new String[result_json.length() - 1];
                    mileage = new String[result_json.length() - 1];

                    for (int i = 1; i < result_json.length(); i++) {
                        try {
                            line = result_json.getString(i).replace('/', ' ');
                        } catch (Exception e) {
                            e.printStackTrace();
                            data_issue = true;
                        }

                        if (line != null) {
                            try {
                                JSONObject jsonobje = new JSONObject(line);
                                if (jsonobje.length() > 0) {
                                    String date_ = jsonobje.getString("date").trim();
                                    date_ = date_.replace(' ','/');
                                    String lat_ = jsonobje.getString("lat");
                                    String lng_ = jsonobje.getString("lng");
                                    String time_ = jsonobje.getString("time");
                                    String speed_ = jsonobje.getString("speed");
                                    double direction_ = jsonobje.getDouble("direction");
                                    String voltage_ = jsonobje.getString("battery");
                                    String event_ = jsonobje.getString("event");
                                    String fuel_ = jsonobje.getString("fuel");
                                    String mileage_ = jsonobje.getString("mileage");

                                    lat[i-1]=(ConvertUtil.getDouble(lat_, "0.0"));
                                    lng[i-1]=(ConvertUtil.getDouble(lng_, "0.0"));
                                    date[i-1]=date_;
                                    time[i-1]=time_;
                                    speed[i-1]=(speed_);
                                    direction[i-1]=direction_;
                                    voltage[i-1]=(voltage_);
                                    event[i-1]=(event_);
                                    fuel[i-1]=(fuel_);
                                    mileage[i-1]=(mileage_);
                                }
                            }catch (Exception ex) {
                                ex.printStackTrace();
                                data_issue = true;
                            }
                        } else {
                            // Log.w(TAG, "Server data issue: " + line );
                            Toast.makeText(getContext(), "Server data issue", Toast.LENGTH_SHORT).show();
                            data_issue = true;
                        }
                    }
                    loadMap();
                } else {
                    no_data = true;
                }
            } else {
                Log.w(TAG, "result_json is null");
                no_data = true;
            }
            // Log.d("-------------------","plotpoints");
        } else {
            no_data = true;
        }

        if(no_data){
            if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    final AlertDialog NoDataAlert = new AlertDialog.Builder(getActivity()).create();
                    NoDataAlert.setMessage(getString(R.string.no_data_available));
                    NoDataAlert.setButton(AlertDialog.BUTTON_POSITIVE, getActivity().getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    NoDataAlert.show();
                }
            }
        } else if(data_issue){
            if(getActivity() == null) {
                if (!getActivity().isFinishing()) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialogBuilder.setTitle(R.string.data_sync_issue);
                    alertDialogBuilder.setMessage(R.string.re_login_and_try_again);
                    alertDialogBuilder.setNeutralButton(getActivity().getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialogBuilder.show();
                }
            }
        }
        p_bar.setVisibility(View.GONE);
    }

    public void loadMap(){
        try {
            p_bar.setVisibility(View.VISIBLE);
            // activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            if (mapView != null) {
                try {
                    mapView.onResume(); // needed to get the map to display immediately
                } catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    MapsInitializer.initialize(getActivity().getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                builder = new LatLngBounds.Builder();

                try {
                    mapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(final GoogleMap mMap) {
                            googleMap = mMap;
                            googleMap.getUiSettings().setRotateGesturesEnabled(false);
                            googleMap.setIndoorEnabled(false);

                            /* try {
                                // Customise the styling of the base map using a JSON object defined
                                // in a raw resource file.
                                boolean success = googleMap.setMapStyle(
                                        MapStyleOptions.loadRawResourceStyle(
                                                getContext(), R.raw.map_json));

                                if (!success) {
                                    Log.e(TAG, "Style parsing failed.");
                                }
                            } catch (Resources.NotFoundException e) {
                                Log.e(TAG, "Can't find style. Error: ", e);
                            } */

                            if (pref.getString("mapSelect", "") != null) {
                                switch (pref.getString("mapSelect", "")) {

                                    case "Normal":
                                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                        rb_normal.setSelected(true);
                                        rb_satellite.setSelected(false);
                                        rb_hybrid.setSelected(false);
                                        editor.putString("mapSelect", "Normal");
                                        editor.commit();
                                        break;
                                    case "Satellite":
                                        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                                        rb_satellite.setSelected(true);
                                        rb_normal.setSelected(false);
                                        rb_hybrid.setSelected(false);
                                        editor.putString("mapSelect", "Satellite");
                                        editor.commit();
                                        break;
                                    case "Hybrid":
                                        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                                        rb_hybrid.setSelected(true);
                                        rb_normal.setSelected(false);
                                        rb_satellite.setSelected(false);
                                        editor.putString("mapSelect", "Hybrid");
                                        editor.commit();
                                        break;
                                    default:
                                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                        rb_normal.setSelected(true);
                                        rb_satellite.setSelected(false);
                                        rb_hybrid.setSelected(false);
                                        editor.putString("mapSelect", "Normal");
                                        editor.commit();
                                }
                            } else {
                                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                rb_normal.setSelected(true);
                                rb_satellite.setSelected(false);
                                rb_hybrid.setSelected(false);
                                editor.putString("mapSelect", "Normal");
                                editor.commit();
                            }

                            if (HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "routehistory")) {
                                if (HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Normal")) {
                                    rb_normal.setSelected(true);
                                } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Satellite")) {
                                    rb_satellite.setSelected(true);
                                } else if (HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Hybrid")) {
                                    rb_hybrid.setSelected(true);
                                } else {
                                    rb_normal.setSelected(true);
                                }
                            } else {
                                rb_normal.setSelected(true);
                            }

                            rb_normal.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                    rb_normal.setSelected(true);
                                    rb_satellite.setSelected(false);
                                    rb_hybrid.setSelected(false);

                                    editor.putString("mapSelect", "Normal");
                                    editor.commit();
                                    return true;
                                }
                            });

                            rb_satellite.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                                    rb_satellite.setSelected(true);
                                    rb_normal.setSelected(false);
                                    rb_hybrid.setSelected(false);

                                    editor.putString("mapSelect", "Satellite");
                                    editor.commit();
                                    return true;
                                }
                            });

                            rb_hybrid.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                                    rb_hybrid.setSelected(true);
                                    rb_normal.setSelected(false);
                                    rb_satellite.setSelected(false);

                                    editor.putString("mapSelect", "Hybrid");
                                    editor.commit();
                                    return true;
                                }
                            });

                            mClusterManager = new ClusterManager<MyItem>(getContext(), googleMap);
                            mRenderer = new ItemRenderer();
                            mClusterManager.setRenderer(mRenderer);

                            googleMap.setOnCameraIdleListener(mClusterManager);

                            googleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
                            mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter());
                            googleMap.setOnMarkerClickListener(mClusterManager);
                            googleMap.setOnInfoWindowClickListener(mClusterManager);
                            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                @Override
                                public void onMapClick(LatLng latLng) {
                                    Marker marker = mRenderer.getMarker(clickedMarker);
                                    if (marker != null && marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        // Animate camera to the bounds
                                        if(initialbounds != null){
                                            try {
                                                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(initialbounds, 100));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            initialbounds = null;
                                        }
                                    }
                                }
                            });

                            mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MyItem>() {
                                @Override
                                public boolean onClusterClick(Cluster<MyItem> cluster) {
                                    // Show a toast with some info when the cluster is clicked.
                                    Toast.makeText(getContext(), cluster.getSize() + " points", Toast.LENGTH_SHORT).show();

                                    BoundsCalculate BoundCalculateTask = new BoundsCalculate();
                                    BoundCalculateTask.execute(cluster);

                                    return true;
                                }
                            });

                            mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
                                @Override
                                public boolean onClusterItemClick(MyItem myItem) {
                                    if(getActivity() != null) {
                                        if (!getActivity().isFinishing()) {
                                            ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                                                clusteritemclickbounds clusteritemclickboundsTask = new clusteritemclickbounds();
                                                clusteritemclickboundsTask.execute(myItem);
                                            } else {
                                                Toast.makeText(getContext(), R.string.internet_connectivity_check, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    return true;
                                }
                            });

                            mClusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>() {
                                @Override
                                public void onClusterItemInfoWindowClick(MyItem myItem) {
                                    Marker marker = mRenderer.getMarker(clickedMarker);
                                    if (marker != null && marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        // Animate camera to the bounds
                                        /* if(initialbounds != null){
                                            try {
                                                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(initialbounds, 100));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            initialbounds = null;
                                        } */
                                    }
                                }
                            });

                            try {
                                Clustering clusterTask = new Clustering();
                                clusterTask.execute();
                                // addItems();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "Problem loading points on map.", Toast.LENGTH_LONG).show();
                            }

                            // Draw polyline through async thread
                            DrawPolyline DrawPolylineTask = new DrawPolyline();
                            DrawPolylineTask.execute();

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.w(TAG, "MapView is null");
            }
        }
        catch (Exception e) {
            // Log.d("------------------",e.getMessage());
            e.printStackTrace();
        }
        p_bar.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.routehistory, container, false);
        p_bar=(TextView) rootView.findViewById(R.id.text_p_bar);
        google_maps_api = getResources().getString(R.string.googlemaps_api_key);
        context = getContext();
        pref = getContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        trackhistory_title = (TextView) rootView.findViewById(R.id.trackhistory_title);
        trackhistory_title.setText(R.string.trackhistory);

        isDestroyed = false;

        if(pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "routehistory")) {
            editor.putString("curfile", "routehistory");
            editor.commit();
        }

        zoomminus = (ImageView) rootView.findViewById(R.id.zoomminus);
        zoomplus = (ImageView) rootView.findViewById(R.id.zoomplus);

        progressLayout = (LinearLayout) getActivity().findViewById(R.id.progressLayout);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressbar);
        progressText = (TextView) getActivity().findViewById(R.id.progressText);
        back_btn = (ImageView) rootView.findViewById(R.id.back);

        start_date = pref.getString("startdate", "");
        // end_date = String.valueOf(routehistoryform.values.get(1));
        end_date = pref.getString("enddate", "");
        // data_point = String.valueOf(routehistoryform.values.get(2));
        data_point = pref.getString("datapoint", "");
        // driver = String.valueOf(routehistoryform.values.get(3));
        driver = pref.getString("driver", "");

        // }

        // Back button
        back_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // closeInfowindow();
                if(isInfoWindowOpen()){
                    Toast.makeText(getContext(), "Please close the info window before navigating to another page", Toast.LENGTH_LONG).show();
                } else {
                    Fragment reportsFragment = new routehistoryform();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    editor.putString("CURRENT_TAG", HomeActivity.TAG_TRACKHISTORY_FORM);
                    editor.commit();
                    fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_TRACKHISTORY_FORM);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        });

        rb_normal = (Button) rootView.findViewById(R.id.rb_normal);
        rb_normal.setText(R.string.standard_view);
        rb_satellite = (Button) rootView.findViewById(R.id.rb_satellite);
        rb_satellite.setText(R.string.satellite_view);
        rb_hybrid = (Button) rootView.findViewById(R.id.rb_hybrid);
        rb_hybrid.setText(R.string.hybrid_view);

        if (data_point.equals("STOPS ONLY")) {
            data_point = data_point.replace(" ", "");
        } else if (data_point.equals("IGNITION ON/OFF")) {
            data_point = "IGNITION";
        } else if (data_point.equals("MOVEMENTS ONLY")) {
            data_point = "MOVEMENT";
        }

        if(getActivity() != null) {
            if (!getActivity().isFinishing()) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    try {
                        getTrackhistoryPoints();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), "Unable to load points. Please check your internet connectivity..", Toast.LENGTH_SHORT).show();
                }
            }
        }

        return rootView;
    }

    public String getDatapoint(String spanishDatapoint){
        switch(spanishDatapoint){
            case "TODOS":
                return "ALL";

            case "Sólo se detiene":
                return "STOPS ONLY";

            case "Encendido / apagado":
                return "IGNITION ON/OFF";

            case "SÓLO MOVIMIENTOS":
                return "MOVEMENTS ONLY";
        }
        return "ALL";
    }


    public void getTrackhistoryPoints() {
        String currentServer = pref.getString("currentServer", "");
        String urls[] = getResources().getStringArray(R.array.url);
        if(Util.isValidUrl(currentServer, urls)) {
            String trackhistoryUrl = currentServer + (getString(R.string.trackhistory_url));

            String datapointStr = data_point;

            ArrayList params = new ArrayList();
            Pair pair = new Pair("operation", "routehistory");
            params.add(pair);
            pair = new Pair("driver", driver.replace(" ", ""));
            params.add(pair);
            pair = new Pair("startdate", start_date);
            params.add(pair);
            pair = new Pair("enddate", end_date);
            params.add(pair);
            pair = new Pair("datapoint", datapointStr);
            params.add(pair);
            pair = new Pair("authcode", pref.getString("authcode", ""));
            params.add(pair);

            String postString = CreatePostString.createPostString(params);
            // Log.i("url", trackhistoryUrl);
            // Log.i("param", postString);
            getDataPoints dataPointsTask = new getDataPoints();
            dataPointsTask.execute(trackhistoryUrl, postString);
        } else {
            Toast.makeText(getContext(), R.string.server_connection_issue, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getActivity(), SplashScreen.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    private class ItemRenderer extends DefaultClusterRenderer<MyItem> {
        private final IconGenerator mIconGenerator = new IconGenerator(context);
        private final ImageView mImageView;
        private final int mDimension;

        public ItemRenderer() {
            super(context, googleMap, mClusterManager);

            mImageView = new ImageView(context);
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            // mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            mImageView.setImageResource(R.drawable.trackicon);
            Bitmap icon = mIconGenerator.makeIcon();
            /* if(item.getIndex() == 0){
                try {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("start", 90, 90)));
                } catch (Exception e){
                    e.printStackTrace();
                }

            }else if(item.getIndex() == lat.length - 1){
                try {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("finish", 90, 90)));
                } catch (Exception e){
                    e.printStackTrace();
                }
            } */
            /*try {
             if(resizeMapIcons("blueicon", 90, 90) != null) {
             markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("blueicon", 90, 90)));
             }
             } catch(Exception e){
             e.printStackTrace();
             } */
            try {
                markerOptions.icon(resizeMapIcons("trackicon", 36, 36))
                        .rotation((float) direction[item.getIndex()]);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        /* @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Person p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(p.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            //mClusterImageView.setImageDrawable(multiDrawable);
            //Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        } */

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 50;
        }
    }

    public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter{
        // Use default InfoWindow frame
        // Getting view from the layout file info_window_layout
        View view = getActivity().getLayoutInflater().inflate(R.layout.trackhistory_windowlayout, null);

        @Override
        public View getInfoWindow(Marker arg0) {
            return null;
        }

        @Override
        public View getInfoContents(final Marker marker) {
            // Getting the position from the marker
            LatLng latLng = marker.getPosition();

            view = getActivity().getLayoutInflater().inflate(R.layout.trackhistory_windowlayout, null);
            //TextView tvLat = (TextView) view.findViewById(R.id.tv_lat);
            //TextView tvLng = (TextView) view.findViewById(R.id.tv_lng);
            TextView tvDate = (TextView) view.findViewById(R.id.tv_date);
            // TextView tvTime = (TextView) view.findViewById(R.id.tv_time);
            TextView tvSpeed = (TextView) view.findViewById(R.id.tv_speed);
            TextView tvVoltage = (TextView) view.findViewById(R.id.tv_voltage);
            TextView tvAddress = (TextView) view.findViewById(R.id.tv_address);
            TextView tvEvent = (TextView) view.findViewById(R.id.tv_event);
            TextView tvDirection = (TextView) view.findViewById(R.id.tv_direction);
            TextView tvMileage = (TextView) view.findViewById(R.id.tv_mileage);
            TextView tvFuel = (TextView) view.findViewById(R.id.tv_fuel);
            ImageView tvClose = (ImageView) view.findViewById(R.id.tv_close);
            // mLastShownInfoWindowMarker = marker;

            if(clickedMarker != null) {
                if(clickedMarker.getVoltage() != null) {
                    SpannableStringBuilder voltageString = new SpannableStringBuilder("Voltage: "+clickedMarker.getVoltage() + "%");
                    voltageString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvVoltage.setText(voltageString);
                }

                if(Address != null) {
                    SpannableStringBuilder addressString = new SpannableStringBuilder("Address: "+Address);
                    addressString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvAddress.setText(addressString);
                }

                if(clickedMarker.getDate() != null) {
                    tvDate.setText(clickedMarker.getDate().replaceAll("\\s+","/") + "   " + DateUtil.covertToCivilianTime(clickedMarker.getTime()));
                }

                /* if(clickedMarker.getTime() != null) {
                    // Log.d("time",clickedMarker.getTime());
                    tvTime.setText(Html.fromHtml( "Time: ") + DateUtil.covertToCivilianTime(clickedMarker.getTime()));
                } else {
                    // Log.w(TAG, "time is null at index "+ clickedMarker.getIndex()+" is "+clickedMarker.getTime());
                } */

                if(clickedMarker.getSpeed() != null) {
                    SpannableStringBuilder speedString = new SpannableStringBuilder("Speed: " +clickedMarker.getSpeed() + " mph");
                    speedString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvSpeed.setText(speedString);
                }

                if(clickedMarker.getEvent() != null) {
                    SpannableStringBuilder eventString = new SpannableStringBuilder("Event: " +clickedMarker.getEvent());
                    eventString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvEvent.setText(eventString);
                }

                // if(clickedMarker.getDirection() > 0) {
                SpannableStringBuilder directionString = new SpannableStringBuilder("Direction: " + String.valueOf(clickedMarker.getDirection()));
                directionString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvDirection.setText(directionString);
                // }

                if(clickedMarker.getMileage() != null) {
                    SpannableStringBuilder mileageString = new SpannableStringBuilder("Mileage: " +clickedMarker.getMileage() + " miles");
                    mileageString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvMileage.setText(mileageString);
                }

                if(clickedMarker.getFuel() != null) {
                    SpannableStringBuilder fuelString = new SpannableStringBuilder("Fuel: " +clickedMarker.getFuel() + " litres");
                    fuelString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvFuel.setText(fuelString);
                }

                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mRenderer.getMarker(clickedMarker).hideInfoWindow();
                    }
                });
            }
            // Returning the view containing InfoWindow contents
            return view;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (com.google.android.gms.maps.MapView) rootView.findViewById(R.id.mapView);
        try {
            if (mapView != null) {
                mapView.onCreate(savedInstanceState);
            }
            mapView.onResume(); // needed for getting the map to display immediately
        } catch (Exception e){
            e.printStackTrace();
        }

        isDestroyed = false;
        try {
            if (mapView != null) {
                mapView.onCreate(savedInstanceState);
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        zoomplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(googleMap != null) {
                    float newzoom = googleMap.getCameraPosition().zoom + 1;
                    LatLng centerLatLng = googleMap.getCameraPosition().target;
                    Double latitude = centerLatLng.latitude;
                    Double longitude = centerLatLng.longitude;
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), newzoom));
                }
            }
        });

        zoomminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(googleMap != null) {
                    float newzoom = googleMap.getCameraPosition().zoom - 1;
                    LatLng centerLatLng = googleMap.getCameraPosition().target;
                    Double latitude = centerLatLng.latitude;
                    Double longitude = centerLatLng.longitude;
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), newzoom));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (mapView != null) {
                mapView.onResume();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        isDestroyed = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(progressLayout.getVisibility() == View.VISIBLE)
        {
            progressText.setText("");
            progressLayout.setVisibility(View.GONE);
            trackhistory_title.setVisibility(View.VISIBLE);
            back_btn.setVisibility(View.VISIBLE);
        }

        try {
            if (mapView != null) {
                mapView.onPause();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        isDestroyed = false;
        // logout();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if(progressLayout.getVisibility() == View.VISIBLE)
        {
            progressText.setText("");
            progressLayout.setVisibility(View.GONE);
            trackhistory_title.setVisibility(View.VISIBLE);
            back_btn.setVisibility(View.VISIBLE);
        }

        try {
            if (mapView != null) {
                mapView.onDestroy();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        isDestroyed = true;
        // logout();
    }

    @Override
    public void onStart() {
        super.onStart();
        isDestroyed = false;

        try {
            if (mapView != null) {
                mapView.onStart();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isDestroyed = false;

        try {
            if (mapView != null) {
                mapView.onStop();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        // logout();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        try {
            if (mapView != null) {
                mapView.onLowMemory();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
    }

    public class getDataPoints extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            if(!isDestroyed) {
                progressLayout.setVisibility(View.VISIBLE);
                progressText.setText(R.string.please_wait);
                trackhistory_title.setVisibility(View.GONE);
                back_btn.setVisibility(View.GONE);
            }
        }

        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Boolean data_issue = false;
            if(progressLayout.getVisibility() == View.VISIBLE)
            {
                progressText.setText("");
                progressLayout.setVisibility(View.GONE);
                trackhistory_title.setVisibility(View.VISIBLE);
                back_btn.setVisibility(View.VISIBLE);
            }

            JSONObject respjson = new JSONObject();
            JSONObject data = new JSONObject();
            if(result == null || !result.equals("")) {
                try {
                    respjson = new JSONObject(result);
                    if (respjson.get("status").equals("true")) {
                        data = respjson.getJSONObject("data");
                        String trackhistorydata = data.getString("trackhistorydata");
                        if (trackhistorydata != null && !trackhistorydata.equals("")) {
                            plotPoints(trackhistorydata);
                        } else {
                            plotPoints("");
                        }
                    } else {
                        if(getActivity() != null) {
                            if (!getActivity().isFinishing()) {
                                AlertDialog authcodeError = new AlertDialog.Builder(getActivity()).create();
                                authcodeError.setMessage(getString(R.string.no_data_available));
                                authcodeError.setButton(AlertDialog.BUTTON_NEUTRAL, getActivity().getResources().getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                JSONObject data = new JSONObject();
                                                try {
                                                    // boolean isInserted = dbHelper.setFlag(0, "", "", "", "", "");
                                                    dialog.dismiss();
                                                    // Intent i = new Intent(getActivity(), SplashScreen.class);
                                                    // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    // startActivity(i);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    dialog.dismiss();
                                                }
                                            }
                                        });
                                authcodeError.show();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    data_issue = true;
                }
            } else {
                data_issue = true;
            }

            if(data_issue){
                if(getActivity() != null) {
                    if (!getActivity().isFinishing()) {
                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialogBuilder.setTitle(R.string.data_sync_issue);
                        alertDialogBuilder.setMessage(R.string.re_login_and_try_again);
                        alertDialogBuilder.setNeutralButton(getActivity().getResources().getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder.show();
                    }
                }
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }

    public class BoundsCalculate extends AsyncTask<Cluster<MyItem>,Void,String > implements DialogInterface.OnCancelListener{

        LatLngBounds bounds;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    if (!isDestroyed) {
                        progressLayout.setVisibility(View.VISIBLE);
                        progressText.setText(R.string.please_wait);
                        trackhistory_title.setVisibility(View.GONE);
                        back_btn.setVisibility(View.GONE);
                    }
                }
            }
        }
        @Override
        protected String doInBackground(Cluster<MyItem>[] clusters) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (ClusterItem item : clusters[0].getItems()) {
                builder.include(item.getPosition());
            }
            // Get the LatLngBounds
            bounds = builder.build();
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            if(progressLayout.getVisibility() == View.VISIBLE)
            {
                progressText.setText("");
                progressLayout.setVisibility(View.GONE);
                trackhistory_title.setVisibility(View.VISIBLE);
                back_btn.setVisibility(View.VISIBLE);
            }

            try {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }

    public class Clustering extends AsyncTask<String,Void,String > implements DialogInterface.OnCancelListener{

        LatLngBounds bounds;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            p_bar.setVisibility(View.VISIBLE);
        }
        @Override
        protected String doInBackground(String... strings) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (int i = 0; i < lat.length; i++) {
                if(lat[i] != 0.0 && lng[i] != 0.0) {
                    // if (i == 0 || i == (lat.length - 1)) {
                    builder.include(new LatLng(lat[i], lng[i]));
                    // }
                    MyItem offsetItem = new MyItem(new LatLng(lat[i], lng[i]), i, lat[i], lng[i], date[i], time[i], speed[i], voltage[i], event[i], direction[i], fuel[i], mileage[i]);
                    mClusterManager.addItem(offsetItem);
                } else {
                    // Log.d("0 coordinates", "Index: "+i+" " +lat[i]+" "+lng[i]);
                }
            }

            // for(int i = 0; i < ((lat.length < 4)?lat.length:4); i++){
            // builder.include(new LatLng(lat[lat.length - i - 1], lng[lng.length - i - 1]));
            // }

            // Get the LatLngBounds
            bounds = builder.build();
            return null;
        }



        @Override
        protected void onPostExecute(String result) {
            // Animate camera to the bounds
            p_bar.setVisibility(View.GONE);
            try {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }

    public class clusteritemclickbounds extends AsyncTask<MyItem,Void,String> implements DialogInterface.OnCancelListener{

        LatLngBounds bounds;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            p_bar.setVisibility(View.VISIBLE);
        }
        @Override
        protected String doInBackground(MyItem... myItems) {
            // Does nothing, but you could go into the user's profile page, for example.
            LatLngBounds.Builder builder = LatLngBounds.builder();
            builder.include(myItems[0].getPosition());
            clickedMarker = myItems[0];
            // Get the LatLngBounds
            bounds = builder.build();

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // Animate camera to the bounds
            p_bar.setVisibility(View.GONE);
            initialbounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
            // Animate camera to the bounds
            try {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    // final String url = "http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php?lat=" + lat[clickedMarker.getIndex()] + "&lng=" + lng[clickedMarker.getIndex()];
                    ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        Address = "";
                        try {
                            String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(clickedMarker.getLatitude()) + "," + Double.toString(clickedMarker.getLongitude()) + google_maps_api;
                            getLocationAddress loc = new getLocationAddress();
                            loc.execute(url);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }

    public class LatLngBuilder extends AsyncTask<String , Void, String>
    {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    if (!isDestroyed) {
                        progressLayout.setVisibility(View.VISIBLE);
                        progressText.setText(R.string.please_wait);
                        trackhistory_title.setVisibility(View.GONE);
                        back_btn.setVisibility(View.GONE);
                    }
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if(progressLayout.getVisibility() == View.VISIBLE)
            {
                progressText.setText("");
                progressLayout.setVisibility(View.GONE);
                trackhistory_title.setVisibility(View.VISIBLE);
                back_btn.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addItems() {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (int i = 0; i < lat.length; i++) {
            if(lat[i] != 0.0 && lng[i] != 0.0) {
                if (i == 0 || i == (lat.length - 1)) {
                    builder.include(new LatLng(lat[i], lng[i]));
                }
                MyItem offsetItem = new MyItem(new LatLng(lat[i], lng[i]), i, lat[i], lng[i], date[i], time[i], speed[i], voltage[i], event[i], direction[i], fuel[i], mileage[i]);
                mClusterManager.addItem(offsetItem);
            } else {
                // Log.d("0 coordinates", "Index: "+i+" " +lat[i]+" "+lng[i]);
            }
        }

        // for(int i = 0; i < ((lat.length < 4)?lat.length:4); i++){
        // builder.include(new LatLng(lat[lat.length - i - 1], lng[lng.length - i - 1]));
        // }

        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class DrawPolyline extends AsyncTask<String, Void, Object>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            p_bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(String url[]) {
            // TODO Auto-generated method stub
            PolylineOptions polylineOptions = new PolylineOptions()
                    .width(3)
                    .color(context.getResources().getColor(R.color.colorMatrack));

            if (lat != null && !lat.equals("")) {
                for (int i = 0; i < lat.length; i++) {
                    if(lat[i] != 0.0 && lng[i] != 0.0) {
                        LatLng latLngPoint = new LatLng(lat[i], lng[i]);
                        polylineOptions.add(latLngPoint);
                    }
                }
            }

            return polylineOptions;
        }//close doInBackground

        @Override
        protected void onPostExecute(Object result) {
            p_bar.setVisibility(View.GONE);
            Polyline polyline = googleMap.addPolyline((PolylineOptions) result);
        }
    }

    private class getLocationAddress extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result != null){
                if(result != ""){
                    // Log.i("geocode address: ", result);
                    try {
                        JSONObject addrObject = new JSONObject(result);
                        JSONArray addressArray = addrObject.getJSONArray("results");
                        if (addressArray != null) {
                            if (addressArray.length() > 0) {
                                JSONObject addressObj = addressArray.getJSONObject(0);
                                String addr = "";
                                addr = addressObj.get("formatted_address").toString();
                                if (addr != null && addr != "") {
                                    Address = addr;
                                } else {
                                    String address_components_str = addressObj.get("address_components").toString();
                                    JSONArray address_components_arr = new JSONArray(address_components_str);
                                    if(address_components_arr != null){
                                        for(int i = 0; i < address_components_arr.length(); i++){
                                            try {
                                                JSONObject obj = address_components_arr.getJSONObject(i);
                                                if (obj != null) {
                                                    try {
                                                        String component = obj.get("long_name").toString();
                                                        if (component != null) {
                                                            if(i == 0){
                                                                addr += component;
                                                            } else {
                                                                addr += ", " + component;
                                                            }
                                                        }
                                                    } catch (Exception e){
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }

                                        if(addr != null && addr != ""){
                                            Address = addr;
                                        }
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if(Address != null && Address != "") {
                try{
                    Marker marker = mRenderer.getMarker(clickedMarker);
                    if(marker != null){
                        marker.showInfoWindow();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                getLocationAndroidGeocoder getLocationAndroidGeocoder = new getLocationAndroidGeocoder();
                getLocationAndroidGeocoder.execute();
            }
        }
    }

    public class getLocationAndroidGeocoder extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            /* String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
                e.printStackTrace();
            } */

            Geocoder geoCoder = new Geocoder(context);
            java.util.List<android.location.Address> matches = null;
            try {
                double latitude = lat[clickedMarker.getIndex()];
                double longitude = lng[clickedMarker.getIndex()];
                matches = geoCoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            android.location.Address address = null;
            if(matches != null){
                address = (matches.isEmpty() ? null : matches.get(0));
            }
            String address_full = "";

            if(address != null) {
                if (address.getAddressLine(0) == null) {
                    address_full += (address.getLocality() == null) ? "" : address.getLocality() + " ";
                    address_full += (address.getAdminArea() == null) ? "" : address.getAdminArea() + " ";
                    address_full += (address.getCountryCode() == null) ? "" : address.getCountryCode();
                } else {
                    address_full = address.getAddressLine(0);
                }
            }

            return address_full;

            //return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result != null && result != "") {
                Address = result;
            }

            try{
                Marker marker = mRenderer.getMarker(clickedMarker);
                if(marker != null){
                    marker.showInfoWindow();
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public BitmapDescriptor resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = null;
        if(isAdded()) {
            try {
                imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getContext().getPackageName()));
                // Drawable mapicon = ContextCompat.getDrawable(context, R.drawable.spyteciconfields);
                // imageBitmap = Bitmap.createBitmap(mapicon.getIntrinsicWidth(), mapicon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if (imageBitmap != null) {
            try {
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
                return BitmapDescriptorFactory.fromBitmap(resizedBitmap);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return BitmapDescriptorFactory.fromBitmap(imageBitmap);
    }

    public static void closeInfowindow(){
        if(clickedMarker != null){
            if(mRenderer != null) {
                Marker marker = mRenderer.getMarker(clickedMarker);
                if (marker != null) {
                    if (marker.isInfoWindowShown()) {
                        try {
                            marker.hideInfoWindow();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public static boolean isInfoWindowOpen(){
        try {
            if (clickedMarker != null) {
                if (mRenderer != null) {
                    Marker marker = mRenderer.getMarker(clickedMarker);
                    if (marker != null) {
                        if (marker.isInfoWindowShown()) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable mapicon = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        Bitmap bitmap = Bitmap.createBitmap(mapicon.getIntrinsicWidth(), mapicon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        mapicon.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}