package com.matrackinc.pickuptrack;

import android.util.Log;
import com.matrackinc.pickuptrack.activity.SplashScreen;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;

public class CustomHttpClient {
    /**
     * The time it takes for our client to timeout
     */
    public static final int HTTP_TIMEOUT = 30 * 1000; // milliseconds

    /**
     * Single instance of our HttpClient
     */
    static String ret;
    private static final String TAG = "CustomHttpClient";

    /**
     * Get our single instance of our HttpClient object
     * @return an HttpClient object with connection parameters set
     */
    /**
     * Performs an HTTP Post request to the specified url with the
     * specified parameters.
     *
     * @param url The web address to post the request to
     * @return The result of the request
     * @throws Exception
     */

    // Send a POST request
    public static String executeHttpPost(String url, String postString) throws Exception {
        Log.d("POST Request", url);
        InputStream in = null;
        Request request = null;

        try{
            RequestBody body = new FormEncodingBuilder()
                    .add("payload", postString)
                    .build();
            request = new Request.Builder().url(url).post(body).build();

            Response response = SplashScreen.getClient().newCall(request).execute();
            ret = response.body().string();

            // Get cookieManager content
            //Log.d(url, SplashScreen.coreCookieManager.retCookie(url));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    // Send a GET request
    public static String executeHttpPost(String url) throws Exception {
        Log.d("GET Request", url);
        InputStream in = null;
        Request request = null;

        try{
            request = new Request.Builder().url(url).build();
            Response response = SplashScreen.getClient().newCall(request).execute();
            ret = response.body().string();

            // Get cookieManager content
            Log.d(url, SplashScreen.coreCookieManager.retCookie(url));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
