package com.matrackinc.pickuptrack;

import android.util.Pair;

import java.util.ArrayList;

public class CreatePostString {

    public static String createPostString(ArrayList<Pair> params){
        String postString = "";

        for(int i = 0; i < params.size(); i++) {
            postString += (params.get(i).first + "::" +params.get(i).second);
            if(i < (params.size() - 1)){
                postString += "###";
            }
        }

        return postString;
    }
}
